
package business.features;

import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author Georgeena
 */
public class SiteOpener {
    
    public void openWebsite(String siteLink)
    {
        try
        {
            Process P;
            P = Runtime.getRuntime().exec("cmd /c start " + siteLink);
        }
        
        catch(IOException e)
        {
            JOptionPane.showMessageDialog(null,"Error: " + e);
                    
        }
    }
    
}

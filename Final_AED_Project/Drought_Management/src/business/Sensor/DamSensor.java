
package business.Sensor;

import business.Dam.Dam;
import business.Dam.DamDirectory;
import java.util.ArrayList;


public class DamSensor extends Sensor{
   
  public static ArrayList<Dam> aboveThreshold(DamDirectory damDirectory)
  {
      ArrayList<Dam> abvthresholdlist = new ArrayList<>();
  for (Dam dam: damDirectory.getDamList())
  {
      if(dam.getCurrentLevel() >= dam.getThreshold())
      {
          abvthresholdlist.add(dam);
      }
     
  } return abvthresholdlist;
  }
  
    public static ArrayList<Dam> belowThreshold(DamDirectory damDirectory)
  {
      ArrayList<Dam> belThresholdlist = new ArrayList<>();
  for (Dam dam: damDirectory.getDamList())
  {
      if(dam.getCurrentLevel() < dam.getThreshold())
      {
          belThresholdlist.add(dam);
      }
     
  } return belThresholdlist;
  }
}

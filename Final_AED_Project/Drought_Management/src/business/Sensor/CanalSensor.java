

package business.Sensor;

import business.Canal.Canal;
import business.Canal.CanalDirectory;
import business.Dam.Dam;
import business.Dam.DamDirectory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class CanalSensor extends Sensor{
    

    public static ArrayList<Canal> sendWater(Dam dam) {
        ArrayList<Canal> sortedCanalList = dam.getCanalDirectory().getCanalList();

        Collections.sort(sortedCanalList, new CanalSensor.LengthComparator());
        return sortedCanalList;
        
    }

    static class LengthComparator implements Comparator<Canal> {

        @Override
        public int compare(Canal can1, Canal can2) {
            int len1 = can1.getDistance();
            int len2 = can2.getDistance();

            if (len1 == len2) {
                return 0;
            } else if (len1 > len2) {
                return 1;
            } else {
                return -1;
            }
        }

    }
    
    
     public static void canalChecker(Dam dam, Canal canal) {
        int newAvail;
        int available;
        int canalcap;
        if (dam.getCurrentLevel() > dam.getThreshold()) {
            if (canal.getCapacity() == canal.getDistance() * canal.getCrossSection()) {
                available = dam.getCurrentLevel() - dam.getThreshold();
                canalcap = canal.getCapacity();
                if (available >= canalcap) {
                    newAvail = available - canalcap;
                    dam.setCurrentLevel((newAvail + dam.getThreshold()));
                    canal.setDistanceCovered(canal.getDistance());
                    canal.setCapacity(canal.getCapacity() - canalcap);
                } else if (available < canalcap) {
                    int cCovered = 0;
                    int cCapacity = 0;
                    while (cCapacity < available) {
                        cCovered++;
                        cCapacity = cCovered * canal.getCrossSection();

                    }
                    cCovered = cCovered - 1;
                    cCapacity = (cCovered) * canal.getCrossSection();
                    newAvail = available - cCapacity;
                    dam.setCurrentLevel(newAvail + dam.getThreshold());
                    canal.setDistanceCovered(cCovered);
                    canal.setCapacity(canal.getCapacity() - cCapacity);
                }
            }
        }
    }
    }
    
    

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.Sensor;

/**
 *
 * @author Georgeena
 */
public abstract class Sensor {
    
     public enum SensorType{
        Dam("DamSensor"),
        Canal("CanalSensor");      
        
        
        private String value;
        private SensorType(String value)
        {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
        
    }
    
}

package business;

import business.Enterprise.Enterprise;
import business.Network.Network;
import business.Organization.Organization;
//import business.Role.DamAdminRole;
import business.Role.Role;
import business.Role.SystemAdminRole;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class EcoSystem extends Organization {

    private static EcoSystem business;
    private ArrayList<Network> networkList;
    private boolean  unique=false;

    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    private EcoSystem() {
        super(null);
        networkList = new ArrayList<>();
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public Network createAndAddNetwork() {
        Network network = new Network();
        networkList.add(network);
        return network;
    }
    
    public void removeNetwork(Network n)
     {
         networkList.remove(n);
     }
    
      public boolean checkIfNetworkNameisUnique(String name) {
        for (Network network : networkList) {
            if (network.getName().equals(name)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }

      public boolean checkIfUsernameIsUnique(String username) {
        for (Network network : networkList) {
            if (network.getDamDirectory().getUserAccountDirectory().checkIfUserNameisUnique(username)) {
                for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                    if (enterprise.getUserAccountDirectory().checkIfUserNameisUnique(username)) {
                        for (Organization org : enterprise.getOrganizationDirectory().getOrganizationList()) {
                            if (org.getUserAccountDirectory().checkIfUserNameisUnique(username)) {
                                return true;
                            }
                        }
                       return  true;
                    }
                }
                return true;
            }

        }
        return false;
    }
          
          
      
}

package business.WorkQueue;

import business.Dam.Dam;

/**
 *
 * @author Georgeena
 */
public class WaterLevelRequest extends WorkRequest {
    
    private Dam senderDam;
    private Dam recieverDam;
    private int waterTransfered;


    /**
     * @return the senderDam
     */
    public Dam getSenderDam() {
        return senderDam;
    }

    /**
     * @param senderDam the senderDam to set
     */
    public void setSenderDam(Dam senderDam) {
        this.senderDam = senderDam;
    }

    /**
     * @return the recieverDam
     */
    public Dam getRecieverDam() {
        return recieverDam;
    }

    /**
     * @param recieverDam the recieverDam to set
     */
    public void setRecieverDam(Dam recieverDam) {
        this.recieverDam = recieverDam;
    }

    /**
     * @return the waterTransfered
     */
    public int getWaterTransfered() {
        return waterTransfered;
    }

    /**
     * @param waterTransfered the waterTransfered to set
     */
    public void setWaterTransfered(int waterTransfered) {
        this.waterTransfered = waterTransfered;
    }
    
    
    public  String toString(){
        return recieverDam.getDamName();
    }
}




package business.WorkQueue;

import business.Canal.Canal;

/**
 *
 * @author Georgeena
 */
public class SubsidyRequest extends WorkRequest {
    
     
   private int subsidyRate;
   private int totalSubsidy;
   private Canal currentCanal;

    /**
     * @return the subsidyRate
     */
    public int getSubsidyRate() {
        return subsidyRate;
    }

    /**
     * @param subsidyRate the subsidyRate to set
     */
    public void setSubsidyRate(int subsidyRate) {
        this.subsidyRate = subsidyRate;
    }

    /**
     * @return the totalSubsidy
     */
    public int getTotalSubsidy() {
        return totalSubsidy;
    }

    /**
     * @param totalSubsidy the totalSubsidy to set
     */
    public void setTotalSubsidy(int totalSubsidy) {
        this.totalSubsidy = totalSubsidy;
    }

    /**
     * @return the currentCanal
     */
    public Canal getCurrentCanal() {
        return currentCanal;
    }

    /**
     * @param currentCanal the currentCanal to set
     */
    public void setCurrentCanal(Canal currentCanal) {
        this.currentCanal = currentCanal;
    }
    
    public  String toString(){
        return currentCanal.getCanalName();
    }
    
}

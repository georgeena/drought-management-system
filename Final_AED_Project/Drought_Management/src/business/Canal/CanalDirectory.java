package business.Canal;

import java.util.ArrayList;

/**
 *
 * @author Georgeena
 */
public class CanalDirectory {

    private ArrayList<Canal> canalList;

    public CanalDirectory() {
        canalList = new ArrayList<>();
    }

    public ArrayList<Canal> getCanalList() {
        return canalList;
    }

    public void setCanalList(ArrayList<Canal> canalList) {
        this.canalList = canalList;
    }

    public Canal CreateandAddCanal(String canalName, int distance, int crossSection, int wastage) {
        Canal canal = new Canal(canalName, distance, crossSection, wastage);
        canalList.add(canal);
        return canal;
    }

    public boolean checkIfCanalNameisUnique(String name) {
        for (Canal c : canalList) {
            if (c.getCanalName().equals(name)) {
                return false;
            }
        }
        return true;
    }

    public void removeCanal(Canal c) {
        canalList.remove(c);
    }

}


package business.Canal;


public class Canal {
    
    private String canalName;
    private int distanceCovered;
   private  int capacity;
    private int distance;
    private int crossSection;
    private int wastage;
    
    public Canal (String canalName, int distance, int crossSection, int wastage)
    {
        this.canalName = canalName;
//        this.canalCapacity = canalCapacity;
        this.distance = distance;
        this.crossSection = crossSection;
        this.wastage = wastage;
        capacity= crossSection* distance;
    }

    public String getCanalName() {
        return canalName;
    }

    public void setCanalName(String canalName) {
        this.canalName = canalName;
    }

   

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getCrossSection() {
        return crossSection;
    }

    public void setCrossSection(int crossSection) {
        this.crossSection = crossSection;
    }

    public int getWastage() {
        return wastage;
    }

    public void setWastage(int wastage) {
        this.wastage = wastage;
    }
    
      @Override
    public String toString(){
        return canalName;
    }

    /**
     * @return the distanceCovered
     */
    public int getDistanceCovered() {
        return distanceCovered;
    }

    /**
     * @param distanceCovered the distanceCovered to set
     */
    public void setDistanceCovered(int distanceCovered) {
        this.distanceCovered = distanceCovered;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
       
}

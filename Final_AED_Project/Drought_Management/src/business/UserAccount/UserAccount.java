
package business.UserAccount;

import business.Employee.Employee;
import business.Role.Role;
import business.WorkQueue.WorkQueue;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;


public class UserAccount {
    
    private String userName;
    private String password;
    private Employee employee;
    private Role role;
//    private ImageIcon icon;
    private WorkQueue workQueue;
    byte[] icon;
    
    public UserAccount()
    {
        workQueue = new WorkQueue();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }
 
    @Override
    public String toString() {
        return userName;
    }

     public ImageIcon getIcon() {
        return new ImageIcon(icon);
    }

        public void setIcon(ImageIcon icon) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
       BufferedImage img = new BufferedImage(icon.getImage().getWidth(null), icon.getImage().getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = img.createGraphics();
        g2d.drawImage(icon.getImage(), 0, 0, null);
        ImageIO.write(img, "png", baos);
      this.icon = baos.toByteArray();
     }
}

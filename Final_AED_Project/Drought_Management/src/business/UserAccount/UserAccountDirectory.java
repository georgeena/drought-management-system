 
package business.UserAccount;

import business.Employee.Employee;
import business.Role.Role;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.ImageIcon;

/**
 *
 * @author Georgeena
 */
public class UserAccountDirectory {
    
    private ArrayList<UserAccount> userAccountList;
    
    public UserAccountDirectory()
    {
        this.userAccountList = new ArrayList<>();
    }

    public ArrayList<UserAccount> getUserAccountList() {
        return userAccountList;
    }

     public UserAccount authenticateUser(String userName, String password){
        for (UserAccount ua : userAccountList)
            if (ua.getUserName().equals(userName) && ua.getPassword().equals(password)){
                return ua;
            }
        return null;
    }
    
    public UserAccount createUserAccount(String userName, String password, Employee employee, Role role,ImageIcon icon) throws IOException{
        UserAccount userAccount = new UserAccount();
        userAccount.setUserName(userName);
        userAccount.setPassword(password);
        userAccount.setEmployee(employee);
        userAccount.setRole(role);
        userAccount.setIcon(icon);
        userAccountList.add(userAccount);
        return userAccount;   
    }
    
    public boolean checkIfUserNameisUnique(String userName)
    {
        for(UserAccount ua :userAccountList){
            if(ua.getUserName().equals(userName))
                return false;
        }
        return true;
    }
    
    
    
}

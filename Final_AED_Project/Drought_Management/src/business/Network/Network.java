
package business.Network;

import business.Canal.CanalDirectory;
import business.Dam.DamDirectory;
import business.Enterprise.EnterpriseDirectory;

/**
 *
 * @author Georgeena
 */
public class Network {
    
    private String name;
    private EnterpriseDirectory enterpriseDirectory;
    private DamDirectory damDirectory;
   // private CanalDirectory canalDirectory;
    
    public Network()
    {
        enterpriseDirectory = new EnterpriseDirectory();
        damDirectory = new DamDirectory();
       // canalDirectory = new CanalDirectory();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

    public void setEnterpriseDirectory(EnterpriseDirectory enterpriseDirectory) {
        this.enterpriseDirectory = enterpriseDirectory;
    }

    public DamDirectory getDamDirectory() {
        return damDirectory;
    }

    public void setDamDirectory(DamDirectory damDirectory) {
        this.damDirectory = damDirectory;
    }

    
  
    @Override
    public String toString() {
        return name;
    }
    
    
    
}

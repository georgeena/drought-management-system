
package business.Role;

import business.EcoSystem;
import business.Enterprise.Enterprise;
import business.Network.Network;
import business.Organization.Organization;
import business.Organization.WaterManagementOrganization;
import business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userInterface.WaterManagementRole.WaterManagementWorkAreaJPanel;

/**
 *
 * @author Georgeena
 */
public class WaterManagementRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business,Network network) {
        return  new WaterManagementWorkAreaJPanel(userProcessContainer,business,account,(WaterManagementOrganization)organization,network);
    }
    
     @Override
    public  String toString(){  
        return "Water Management";
    }
    
}

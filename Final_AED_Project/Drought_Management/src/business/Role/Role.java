
package business.Role;

import business.EcoSystem;
import business.Enterprise.Enterprise;
import business.Network.Network;
import business.Organization.Organization;
import business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author Georgeena
 */
public abstract class Role {
    
     
    public enum RoleType{
        DamAdmin("DamAdmin"),
        Admin("Admin"),
        Govt("Government"),
        Consumption("Consumption"),
        WaterManagement("Water Management");
        
       
        private String value;
               private RoleType(String value)
        {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
        
    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account, 
            Organization organization, 
            Enterprise enterprise, 
            EcoSystem business,
            Network network
    );

    
       @Override
    public String toString() {
   // return this.getClass().getName();
        return this.getClass().getTypeName();
    }
   
}

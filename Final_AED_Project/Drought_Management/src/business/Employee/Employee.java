/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package business.Employee;

/**
 *
 * @author Georgeena
 */
public class Employee {
    
    private String name;
    private String age;
    private String phoneNumber;
  //  private String address;
    
    private int id;
    private static int count = 1;
 
    public Employee() {
        id = count;
        count++;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Employee.count = count;
    }
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package business.Employee;

import java.util.ArrayList;

/**
 *
 * @author Georgeena
 */
public class EmployeeDirectory {
    
    private ArrayList<Employee> employeeList;

    public EmployeeDirectory() {
        employeeList = new ArrayList<>();
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }
    
    public Employee createEmployee(String name, String age, String phoneNumber)
    {
        Employee emp = new Employee();
        emp.setName(name);
        emp.setAge(age);
        emp.setPhoneNumber(phoneNumber);
        //emp.setAddress(address);
        
        employeeList.add(emp);
        return emp;
    }
}
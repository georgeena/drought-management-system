
package business.Organization;

import business.Role.Role;
import business.Role.WaterManagementRole;
import java.util.ArrayList;

/**
 *
 * @author Georgeena
 */
public class WaterManagementOrganization extends Organization {
    
    public WaterManagementOrganization()
    {
        super(Organization.Type.Management.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() 
    {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new WaterManagementRole());
        return roles;
    }
         
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.Organization;

import business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author Georgeena
 */
public class OrganizationDirectory {
    
    private ArrayList <Organization> organizationList;
    
    public OrganizationDirectory()
    {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public void setOrganizationList(ArrayList<Organization> organizationList) {
        this.organizationList = organizationList;
    }
    
    public Organization createOrganization(Type type)
    {
        Organization organization = null;
        if(type.getValue().equals(Type.Govt.getValue()))
        {
            organization = new GovtOrganization();
            organizationList.add(organization);
        }
 
        else if
                (type.getValue().equals(Type.Management.getValue()))
        {
            organization = new WaterManagementOrganization();
            organizationList.add(organization);
        }
        return organization;
     
    }
    
            
            
            
            
            
            
            }    
   
 
            
            
    


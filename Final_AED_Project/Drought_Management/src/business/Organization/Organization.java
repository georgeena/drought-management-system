/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.Organization;

import business.Employee.EmployeeDirectory;
import business.Role.Role;
import business.UserAccount.UserAccountDirectory;
import business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author Georgeena
 */
public abstract class Organization {
    
    private String name;
    private UserAccountDirectory userAccountDirectory;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDirectory;
    private int organizationID;
    private static int counter;
    
    public enum Type{
        Admin("Admin Organization"),
        Govt("Government Organization"),
       // Consumption ("Water Consumption Organization"),
        Management("Water Management Organization");
        
        private String value;
        
        private Type (String value)
        {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
        
    }
    
        public Organization(String name)
        {
            this.name=name;
            workQueue = new WorkQueue();
            employeeDirectory = new EmployeeDirectory();
            userAccountDirectory = new UserAccountDirectory();
            organizationID = counter;
            ++counter;
        }
            
        public abstract ArrayList<Role> getSupportedRole();

        
    // GETTERS AND SETTERS
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(int organizationID) {
        this.organizationID = organizationID;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Organization.counter = counter;
    }

    @Override
    public String toString() {
        return name;
    }
        
        
        
    }
            
    
    


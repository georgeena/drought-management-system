


package business.Organization;

import business.Role.GovtRole;
import business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Georgeena
 */
public class GovtOrganization extends Organization {
    
    public GovtOrganization()
    {
        super(Organization.Type.Govt.getValue());
        
    }

    @Override
    public ArrayList<Role> getSupportedRole() 
    {
       ArrayList<Role> roles = new ArrayList<>();
       roles.add(new GovtRole());
       return roles;
    }
}


package business.Dam;

import business.Canal.CanalDirectory;
import business.Employee.Employee;
import business.Employee.EmployeeDirectory;
import business.UserAccount.UserAccount;
import business.UserAccount.UserAccountDirectory;
import java.util.ArrayList;

/**
 *
 * @author Georgeena
 */
public class DamDirectory {
    
    private ArrayList<Dam> damList;
   private UserAccountDirectory userAccountDirectory;
   private EmployeeDirectory employeeDirectory;
    
    public DamDirectory()
    {
        damList = new ArrayList<>();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
    }

    public ArrayList<Dam> getDamList() {
        return damList;
    }

    public void setDamList(ArrayList<Dam> damList) {
        this.damList = damList;
    }
    
     public Dam createAndAddDam(String name, int capacity)
     {
         Dam dam = new Dam(name, capacity);
         damList.add(dam);
         return dam;
         
     }
    
     public void removeDam(Dam d)
     {
         damList.remove(d);
     }

    /**
     * @return the userAccountDirectory
     */
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    /**
     * @param userAccountDirectory the userAccountDirectory to set
     */
    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }

    /**
     * @return the employeeDirectory
     */
    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    /**
     * @param employeeDirectory the employeeDirectory to set
     */
    public void setEmployeeDirectory(EmployeeDirectory employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

       public boolean checkIfDamNameisUnique(String name) {
        for (Dam dam : damList) {
            if (dam.getDamName().equals(name)) {
                return false;
            }
        }
        return true;
    }
    
  
}
   


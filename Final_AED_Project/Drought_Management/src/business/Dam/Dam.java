

package business.Dam;

import business.Canal.CanalDirectory;

public class Dam  {
    
    private String damName;
    private int threshold ;
    private int capacity ;
    private int currentLevel;
    private CanalDirectory canalDirectory;
    
    
    public Dam(String damName,int capacity)
    {
    this.damName=damName;
    this.capacity= capacity;
    threshold= (40*capacity)/100;
    canalDirectory = new CanalDirectory();
    }

    public String getDamName() {
        return damName;
    }

    public void setDamName(String damName) {
        this.damName = damName;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public CanalDirectory getCanalDirectory() {
        return canalDirectory;
    }

    public void setCanalDirectory(CanalDirectory canalDirectory) {
        this.canalDirectory = canalDirectory;
    }

   
    
    @Override
    public String toString(){
     return damName;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the currentLevel
     */
    public int getCurrentLevel() {
        return currentLevel;
    }

    /**
     * @param currentLevel the currentLevel to set
     */
    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }
    
    
}

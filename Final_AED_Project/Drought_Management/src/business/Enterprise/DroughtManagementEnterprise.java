
package business.Enterprise;

import business.Enterprise.Enterprise.EnterpriseType;
import business.Role.Role;
import java.util.ArrayList;


public class DroughtManagementEnterprise extends Enterprise{

    public DroughtManagementEnterprise(String name) {
        super(name, EnterpriseType.DroughtManagement);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
    
    
    
}

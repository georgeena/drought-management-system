package userInterface.GovtRole;

import business.Canal.Canal;
import business.Dam.Dam;
import business.EcoSystem;
import business.Network.Network;
import business.Organization.GovtOrganization;
import business.Organization.Organization;
import business.Sensor.CanalSensor;
import business.UserAccount.UserAccount;
import business.WorkQueue.SubsidyRequest;
import business.WorkQueue.WorkRequest;
import business.features.SendEmail;
import java.awt.CardLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


public class GovtAreaWorkPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Organization organization;
    private EcoSystem business;
    private UserAccount account;
    private Network network;
    private Dam dam;
    private SubsidyRequest request;

    public GovtAreaWorkPanel(JPanel userProcessContainer, UserAccount account, GovtOrganization organization, EcoSystem business, Network network) 
    {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.account = account;
        this.business = business;
        valueLabel.setText(organization.getName());
       // imgLabel.setIcon(account.getIcon());
        nameLabel.setText(account.getEmployee().getName());
        this.network = network;
        populateDamComboBox();
        populateSubTable();
        populateGovtTable();

        Image icon = account.getIcon().getImage();
        icon = icon.getScaledInstance(218, 256, Image.SCALE_SMOOTH);

        imgLabel.setIcon(new ImageIcon(icon));
        
        
        
    }

    private void populateSubTable() {
        DefaultTableModel model = (DefaultTableModel) tableSubsidy.getModel();
        model.setRowCount(0);
        for (WorkRequest request : account.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[5];
            row[0] = ((SubsidyRequest) request);
            row[1] = (((SubsidyRequest) request).getSubsidyRate());
            row[2] = ((SubsidyRequest) request).getTotalSubsidy();
            row[3] = request.getReceiver();
            row[4] = request.getStatus();

            model.addRow(row);
        }

    }

    private void populateGovtTable() {
        DefaultTableModel model = (DefaultTableModel) tableGovt.getModel();
        if (dam == null) {
            model.setRowCount(0);

        } else {

            model.setRowCount(0);
            for (Canal canal : CanalSensor.sendWater(dam)) {
                Object[] row = new Object[6];

                row[0] = dam.getDamName();
                row[1] = (dam.getCurrentLevel() - dam.getThreshold());
                row[2] = canal;
                row[3] = canal.getDistance();
                row[4] = canal.getCrossSection();
                CanalSensor.canalChecker(dam, canal);
                row[5] = canal.getDistanceCovered();//tentative(have to change later)
                model.addRow(row);
            }
        }
    }

    private void populateDamComboBox() {
        if (comboBoxDamName.getItemCount() != 0) {
            comboBoxDamName.removeAllItems();
        }

        for (Dam dam : network.getDamDirectory().getDamList()) {
            comboBoxDamName.addItem(dam);
        }
//        comboBoxDamName.setSelectedIndex(0);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        organizationLabel = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();
        imgLabel = new javax.swing.JLabel();
        nameLabel = new javax.swing.JLabel();
        welcomeLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableGovt = new javax.swing.JTable();
        comboBoxDamName = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableSubsidy = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 204));

        organizationLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        organizationLabel.setText("Organization :");

        valueLabel.setText("<value>");

        nameLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        welcomeLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        welcomeLabel.setText("Welcome ");

        tableGovt.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Dam Nam", "Water Available ", "Canal Name", "Canal Length", "Canal Cross Section", "Canal Length Covered"
            }
        ));
        jScrollPane1.setViewportView(tableGovt);

        comboBoxDamName.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboBoxDamName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxDamNameActionPerformed(evt);
            }
        });

        jButton1.setText("Refresh");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        tableSubsidy.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Canal", "Subsidy Rate", "Subsidy Raised", "Resolved By", "Status"
            }
        ));
        jScrollPane2.setViewportView(tableSubsidy);

        jButton2.setText("Assign to Me");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Resolve");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(comboBoxDamName, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(53, 53, 53)
                                .addComponent(jButton1))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(organizationLabel)
                                    .addComponent(welcomeLabel))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(imgLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 766, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 619, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2)
                            .addComponent(jButton3))
                        .addContainerGap(287, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(organizationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(welcomeLabel)
                                .addGap(23, 23, 23))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(nameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(comboBoxDamName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1))
                        .addGap(31, 31, 31)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2))
                    .addComponent(imgLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton3)
                .addContainerGap(25, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxDamNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxDamNameActionPerformed

//        if (comboBoxDamName.getItemCount()!= 0)

    }//GEN-LAST:event_comboBoxDamNameActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        dam = (Dam) comboBoxDamName.getSelectedItem();
        populateGovtTable();
        populateSubTable();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (tableGovt.getRowCount() != 0) {
            int selectedrow = tableGovt.getSelectedRow();
            if(selectedrow <0){
                return;
            }
            Canal canal = (Canal) tableGovt.getValueAt(selectedrow, 2);
            if (canal.getDistanceCovered() != canal.getDistance()) {
                request = new SubsidyRequest();
                request.setCurrentCanal(canal);
                request.setReceiver(account);
                request.setStatus("Pending");

                organization.getWorkQueue().getWorkRequestList().add(request);
                account.getWorkQueue().getWorkRequestList().add(request);

                populateSubTable();

            }else{
                JOptionPane.showMessageDialog(null,"Cannot raise a request !","Warning",JOptionPane.WARNING_MESSAGE);
            }
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
         int selectedRow = tableSubsidy.getSelectedRow();
        
         if (selectedRow < 0){
         return;
         }
         
        
        this.request = (SubsidyRequest)tableSubsidy.getValueAt(selectedRow, 0);
             
//         request.setStatus("Processing");
//         */

        //request.setStatus("Processing");
        if(request.getTotalSubsidy()== 0){
        RaiseSubsidyJPanel raiseSubsidyJPanel= new RaiseSubsidyJPanel(userProcessContainer, business, account, (GovtOrganization) organization, network, request);
        userProcessContainer.add("raiseSubsidyJPanel", raiseSubsidyJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
         } else{
             JOptionPane.showMessageDialog(null,"Request has been completed !","Warning",JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox comboBoxDamName;
    private javax.swing.JLabel imgLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JLabel organizationLabel;
    private javax.swing.JTable tableGovt;
    private javax.swing.JTable tableSubsidy;
    private javax.swing.JLabel valueLabel;
    private javax.swing.JLabel welcomeLabel;
    // End of variables declaration//GEN-END:variables

}

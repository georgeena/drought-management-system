package userInterface.SystemAdminRole;

import business.Canal.Canal;
import business.Dam.Dam;
import business.EcoSystem;
import business.Network.Network;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Georgeena
 */
public class ManageDamJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private EcoSystem system;
    private Dam dam;

    public ManageDamJPanel(JPanel userProcessContainer, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.system = system;

        populateDamTable();
        populateComboBox();
        
      //  populateCanalTable();
    }

    private void populateDamTable() {
        DefaultTableModel model = (DefaultTableModel) tableDam.getModel();

        model.setRowCount(0);
        for (Network network : system.getNetworkList()) {
            
            for (Dam dam : network.getDamDirectory().getDamList()) {
               

                    Object[] row = new Object[3];
                    row[0] = network.getName();
                    row[1] = dam;
                    row[2] = dam.getCapacity();
                    
                    model.addRow(row);
                
            }
        }
    }
    
     private void populateCanalTable() {
         DefaultTableModel model = (DefaultTableModel) tableCanal.getModel();
         model.setRowCount(0);
         for(Canal canal: dam.getCanalDirectory().getCanalList()){
              Object[] row = new Object[4];
                    row[0] = canal;
                    row[1] = canal.getDistance();
                    row[2]= canal.getCrossSection();
                    row[3] = canal.getWastage();
                    
                    model.addRow(row);
         }
             
       
    }

    private void populateComboBox() {
        comboBoxNetwork.removeAllItems();

        for (Network network : system.getNetworkList()) {
            comboBoxNetwork.addItem(network);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableDam = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        comboBoxNetwork = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        txtDam = new javax.swing.JTextField();
        backJButton = new javax.swing.JButton();
        submitJButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtCanalName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCapacity = new javax.swing.JTextField();
        txtDistanceCanal = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtCrossSection = new javax.swing.JTextField();
        txtwastage = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        AddjButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableCanal = new javax.swing.JTable();
        searchjButton = new javax.swing.JButton();
        searchjTextField = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 204));

        tableDam.setBackground(new java.awt.Color(11, 32, 73));
        tableDam.setFont(new java.awt.Font("sansserif", 1, 12)); // NOI18N
        tableDam.setForeground(new java.awt.Color(255, 255, 204));
        tableDam.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Network", "Dam Name", "Capacity"
            }
        ));
        tableDam.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableDamMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableDam);

        jLabel1.setText("Network");

        comboBoxNetwork.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboBoxNetwork.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxNetworkActionPerformed(evt);
            }
        });

        jLabel2.setText("Name");

        backJButton.setText("<< Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        submitJButton.setText("Submit");
        submitJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitJButtonActionPerformed(evt);
            }
        });

        jLabel3.setText("Associated Canal");

        txtCanalName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCanalNameActionPerformed(evt);
            }
        });

        jLabel4.setText("Capacity");

        txtCapacity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCapacityActionPerformed(evt);
            }
        });

        txtDistanceCanal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDistanceCanalActionPerformed(evt);
            }
        });

        jLabel5.setText("Canal Distance");

        jLabel6.setText("Cross Section");

        jLabel8.setText("Wastage");

        AddjButton.setText("Add");
        AddjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddjButtonActionPerformed(evt);
            }
        });

        tableCanal.setBackground(new java.awt.Color(11, 32, 73));
        tableCanal.setForeground(new java.awt.Color(255, 255, 204));
        tableCanal.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Canal Name", "Distance", "Cross Section", "Wastage"
            }
        ));
        tableCanal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableCanalMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tableCanal);

        searchjButton.setText("Search");
        searchjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchjButtonActionPerformed(evt);
            }
        });

        searchjTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchjTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 493, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel5)
                                    .addComponent(backJButton))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtDistanceCanal, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCanalName, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(46, 46, 46)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel6))
                                .addGap(35, 35, 35)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtwastage, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(AddjButton))
                                    .addComponent(txtCrossSection, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(txtDam, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(37, 37, 37)
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(txtCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(110, 110, 110)
                                .addComponent(submitJButton))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboBoxNetwork, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(108, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(searchjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(searchjButton)
                .addGap(167, 167, 167))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchjButton))
                .addGap(9, 9, 9)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxNetwork, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(submitJButton)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtDam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(txtCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)))
                .addGap(29, 29, 29)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCanalName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtDistanceCanal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtCrossSection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(txtwastage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(AddjButton))))
                .addGap(18, 18, 18)
                .addComponent(backJButton)
                .addContainerGap(38, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        SystemAdminWorkAreaJPanel sysAdminwjp = (SystemAdminWorkAreaJPanel) component;
        sysAdminwjp.populateTree();

        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed

    private void submitJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitJButtonActionPerformed

        Network network = (Network) comboBoxNetwork.getSelectedItem();
        // Enterprise.EnterpriseType type = (Enterprise.EnterpriseType) enterpriseTypeJComboBox.getSelectedItem();

        if (network == null) //|| type == null) {
        {
            JOptionPane.showMessageDialog(null, "Invalid Input!");
            return;
        }

        String damName = txtDam.getText();
        int capacity = Integer.parseInt(txtCapacity.getText());
        
      if(network.getDamDirectory().checkIfDamNameisUnique(damName)){
        Dam dam = network.getDamDirectory().createAndAddDam(damName, capacity);
      }else{
          JOptionPane.showMessageDialog(null,damName+" already exists!","Information",JOptionPane.INFORMATION_MESSAGE);
      }

       // Enterprise enterprise = network.getEnterpriseDirectory().createAndAddEnterprise(name, type);
        populateDamTable();
    }//GEN-LAST:event_submitJButtonActionPerformed

    private void comboBoxNetworkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxNetworkActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBoxNetworkActionPerformed

    private void txtCapacityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCapacityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCapacityActionPerformed

    private void txtDistanceCanalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDistanceCanalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDistanceCanalActionPerformed

    private void txtCanalNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCanalNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCanalNameActionPerformed

    private void AddjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddjButtonActionPerformed
      int selectedrow = tableDam.getSelectedRow();
       dam = (Dam)tableDam.getValueAt(selectedrow,1);
        
        String canalName = txtCanalName.getText();
        int distance = Integer.parseInt(txtDistanceCanal.getText());
        int crossSection = Integer.parseInt(txtCrossSection.getText());
        int wastage = Integer.parseInt(txtwastage.getText());
        
        if(dam.getCanalDirectory().checkIfCanalNameisUnique(canalName)){
        Canal canal = dam.getCanalDirectory().CreateandAddCanal(canalName,distance, crossSection, wastage);
        }else{
            JOptionPane.showMessageDialog(null,canalName +" already exists!","Information",JOptionPane.INFORMATION_MESSAGE);
        }
        
        populateCanalTable();
        
    }//GEN-LAST:event_AddjButtonActionPerformed

    private void tableCanalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableCanalMouseClicked
        
    }//GEN-LAST:event_tableCanalMouseClicked

    private void tableDamMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableDamMouseClicked
       int selectedrow = tableDam.getSelectedRow();
          dam = (Dam)tableDam.getValueAt(selectedrow,1);
          populateCanalTable();
    }//GEN-LAST:event_tableDamMouseClicked

    private void searchjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchjButtonActionPerformed
//         for (Network n : system.getNetworkList()) {
//            for (Enterprise enterprise : n.getEnterpriseDirectory().getEnterpriseList()) {
//                if (enterprise.getName().equals(searchjTextField.getText())) {
//                    DefaultTableModel model = (DefaultTableModel) enterpriseJTable.getModel();
//                    model.setRowCount(0);
//                    Object[] row = new Object[3];
//                    row[0] = enterprise.getName();
//                    row[1] = n.getName();
//                    row[2] = enterprise.getEnterpriseType().getValue();
//
//                    model.addRow(row);
//
//                }
//            }
//        }
    }//GEN-LAST:event_searchjButtonActionPerformed

    private void searchjTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchjTextFieldActionPerformed

    }//GEN-LAST:event_searchjTextFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddjButton;
    private javax.swing.JButton backJButton;
    private javax.swing.JComboBox comboBoxNetwork;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton searchjButton;
    private javax.swing.JTextField searchjTextField;
    private javax.swing.JButton submitJButton;
    private javax.swing.JTable tableCanal;
    private javax.swing.JTable tableDam;
    private javax.swing.JTextField txtCanalName;
    private javax.swing.JTextField txtCapacity;
    private javax.swing.JTextField txtCrossSection;
    private javax.swing.JTextField txtDam;
    private javax.swing.JTextField txtDistanceCanal;
    private javax.swing.JTextField txtwastage;
    // End of variables declaration//GEN-END:variables

   
}

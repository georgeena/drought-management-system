/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.WaterManagementRole;

import business.Dam.Dam;
import business.EcoSystem;
import business.Network.Network;
import business.Organization.Organization;
import business.Organization.WaterManagementOrganization;
import business.Sensor.DamSensor;
import business.UserAccount.UserAccount;
import business.WorkQueue.WaterLevelRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Georgeena
 */
public class ResolveIssueJPanel extends javax.swing.JPanel {

   private JPanel userProcessContainer;
    private EcoSystem business;
    private UserAccount account;
    private Organization organization;
    private  Network network;
    private  WaterLevelRequest request;
    private  int availability;
    private int defeciency;
    
     public ResolveIssueJPanel(JPanel userProcessContainer,EcoSystem business,UserAccount account,WaterManagementOrganization organization,Network network, WaterLevelRequest request) {
        initComponents();
       this.userProcessContainer= userProcessContainer;
       this.business = business;
       this.account = account;
       this.organization=organization;
       this.network = network;
       this.request= request;
       //valueLabel.setText(organization.getName());
       //imgLabel.setIcon(account.getIcon());
       //nameLabel.setText(account.getEmployee().getName());
       populateDamTable();
       populateTable();
             
    }
    
    
     private void populateTable()
    {
        DefaultTableModel model = (DefaultTableModel) tableDam.getModel();
        
        model.setRowCount(0);
        
          for (Dam dam :DamSensor.aboveThreshold(network.getDamDirectory())) 
            {                 
                
                Object[] row = new Object[2];
                row[0] = dam;
                
                this.availability = dam.getCurrentLevel() -(( 10*(dam.getCapacity())/100)+ dam.getThreshold());
                if(availability>0)
                row[1]=availability;
           
                                       
                model.addRow(row);
            }
        }
     
     private void populateDamTable(){
          DefaultTableModel model = (DefaultTableModel) requestTable.getModel();
        model.setRowCount(0);
        Object[] row = new Object[3];
         row[0]= request;
        row[1]=request.getRecieverDam().getCurrentLevel();
        Dam d = request.getRecieverDam();
        this.defeciency = (d.getThreshold()+ (10*(d.getCapacity())/100) - d.getCurrentLevel());
        row[2]= defeciency;
       
        model.addRow(row);
     }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        backJButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableDam = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        requestTable = new javax.swing.JTable();
        transferJButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 204));

        jLabel1.setText("Dams above Threshold");

        backJButton.setText("<< Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        tableDam.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Name", "Water Available for Transfer"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableDam);

        requestTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "Dam", "Water Level", "Water Deficiency"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(requestTable);

        transferJButton.setText("Transfer");
        transferJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transferJButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(backJButton)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(349, 349, 349)
                        .addComponent(transferJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(56, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(transferJButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(backJButton)
                .addContainerGap(50, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed

    private void transferJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transferJButtonActionPerformed
        int selectedRow = tableDam.getSelectedRow();
        
         if (selectedRow < 0){
         return;
         }
        int temp;
        Dam sDam= (Dam)tableDam.getValueAt(selectedRow,0);
         Dam rDam = request.getRecieverDam();
        availability = (int)tableDam.getValueAt(selectedRow,1);
        defeciency = (int)requestTable.getValueAt(0,2);
        if(defeciency> availability)
            JOptionPane.showMessageDialog(null,"Water Shortage! Request can't be fulfilled.","Warning",JOptionPane.WARNING_MESSAGE);
        else
        {
           temp = sDam.getCurrentLevel()- defeciency;
           sDam.setCurrentLevel(temp);
           temp = rDam.getCurrentLevel()+defeciency;
           rDam.setCurrentLevel(temp);
           request.setStatus("Completed");
           request.setSenderDam(sDam);
           request.setWaterTransfered(defeciency);
        }
        
       
        
        populateDamTable();
        populateTable();
    }//GEN-LAST:event_transferJButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backJButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable requestTable;
    private javax.swing.JTable tableDam;
    private javax.swing.JButton transferJButton;
    // End of variables declaration//GEN-END:variables
}

 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userInterface.WaterManagementRole;

import business.Dam.Dam;
import business.EcoSystem;
import business.Network.Network;
import business.Organization.Organization;
import business.Organization.WaterManagementOrganization;
import business.Sensor.DamSensor;
import business.UserAccount.UserAccount;
import business.WorkQueue.WaterLevelRequest;
import business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Georgeena
 */
public class WaterManagementWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form WaterManagementWorkAreaJpanel
     */
    private JPanel userProcessContainer;
    private EcoSystem business;
    private UserAccount account;
    private Organization organization;
    private Network network;
    private WaterLevelRequest request;
//    private boolean  noAction= false;

    public WaterManagementWorkAreaJPanel(JPanel userProcessContainer, EcoSystem business, UserAccount account, WaterManagementOrganization organization, Network network) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        this.account = account;
        this.organization = organization;
        this.network = network;
        valueLabel.setText(organization.getName());
//        imgLabel.setIcon(account.getIcon());
        nameLabel.setText(account.getEmployee().getName());
        
        Image icon = account.getIcon().getImage();
        icon = icon.getScaledInstance(218, 256, Image.SCALE_SMOOTH);

        imgLabel.setIcon(new ImageIcon(icon));

        populateComboBox();
        populateTableWater();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableWater = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        damComboBox = new javax.swing.JComboBox();
        assignjbutton = new javax.swing.JButton();
        resolvejbutton = new javax.swing.JButton();
        valueLabel = new javax.swing.JLabel();
        imgLabel = new javax.swing.JLabel();
        nameLabel = new javax.swing.JLabel();
        welcomeLabel = new javax.swing.JLabel();
        organizationLabel = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 204));

        tableWater.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "From Dam", "To Dam", "Water Transfered", "Resolved by", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableWater);

        jLabel1.setText("Dams  below Threshold");

        damComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        damComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                damComboBoxActionPerformed(evt);
            }
        });

        assignjbutton.setText("Assign to me");
        assignjbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignjbuttonActionPerformed(evt);
            }
        });

        resolvejbutton.setText("Resolve Issue");
        resolvejbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resolvejbuttonActionPerformed(evt);
            }
        });

        valueLabel.setText("<value>");

        nameLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        welcomeLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        welcomeLabel.setText("WELCOME");

        organizationLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        organizationLabel.setText("Organization :");

        jButton1.setText("Refresh");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(damComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(assignjbutton)
                        .addGap(18, 18, 18)
                        .addComponent(resolvejbutton))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(welcomeLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(nameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(151, 151, 151)
                                .addComponent(jButton1))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(organizationLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 516, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(101, 101, 101)
                        .addComponent(imgLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(67, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(organizationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(welcomeLabel)
                                    .addComponent(nameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jButton1))
                        .addGap(29, 29, 29)
                        .addComponent(jLabel1))
                    .addComponent(imgLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(damComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(assignjbutton)
                    .addComponent(resolvejbutton))
                .addContainerGap(380, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void resolvejbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resolvejbuttonActionPerformed
        
         int selectedRow = tableWater.getSelectedRow();
        
         if (selectedRow < 0){
         return;
         }
        
        this.request = (WaterLevelRequest)tableWater.getValueAt(selectedRow, 1);
     
//         request.setStatus("Processing");
//         */

        //request.setStatus("Processing");
        
        ResolveIssueJPanel resolveIssueJPanel = new ResolveIssueJPanel(userProcessContainer, business, account, (WaterManagementOrganization) organization, network, request);
        userProcessContainer.add("resolveIssueJPanel", resolveIssueJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        

    }//GEN-LAST:event_resolvejbuttonActionPerformed

    private void assignjbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignjbuttonActionPerformed
       if(damComboBox.getItemCount()!=0)
       {
        request = new WaterLevelRequest();
        request.setReceiver(account);
        request.setRecieverDam((Dam) damComboBox.getSelectedItem());
        request.setStatus("Pending");
        organization.getWorkQueue().getWorkRequestList().add(request);
        account.getWorkQueue().getWorkRequestList().add(request);
        
        populateTableWater(); 
       }
    }//GEN-LAST:event_assignjbuttonActionPerformed

    private void damComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_damComboBoxActionPerformed

    }//GEN-LAST:event_damComboBoxActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

    populateTableWater();
    populateComboBox();
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void populateComboBox() {

        //      noAction = true;
        if (damComboBox.getItemCount() != 0) {
            damComboBox.removeAllItems();
        
        for (Dam dam : DamSensor.belowThreshold(network.getDamDirectory())) {
            damComboBox.addItem(dam);
        }
        }
//        noAction = false;
//        damComboBox.setSelectedIndex(0);
    }

    private void populateTableWater() {
        DefaultTableModel model = (DefaultTableModel) tableWater.getModel();
        model.setRowCount(0);
        for (WorkRequest request : account.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[5];
            row[0] = ((WaterLevelRequest) request).getSenderDam();
            row[1] = ((WaterLevelRequest) request);
            row[2] = ((WaterLevelRequest) request).getWaterTransfered();
            row[3] = request.getReceiver();
            row[4] = request.getStatus();

            model.addRow(row);
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignjbutton;
    private javax.swing.JComboBox damComboBox;
    private javax.swing.JLabel imgLabel;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JLabel organizationLabel;
    private javax.swing.JButton resolvejbutton;
    private javax.swing.JTable tableWater;
    private javax.swing.JLabel valueLabel;
    private javax.swing.JLabel welcomeLabel;
    // End of variables declaration//GEN-END:variables

}
